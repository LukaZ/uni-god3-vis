import pandas as pd
import numpy as np
from sklearn import datasets
import statsmodels.api as sm

#https://towardsdatascience.com/simple-and-multiple-linear-regression-in-python-c928425168f9
##dataFrame = pd.read_csv('./data/United_States_COVID-19_Cases_and_Deaths_by_State_over_Time.csv')
##print(csv)

data = datasets.load_boston()

# define the data/predictors as the pre-set feature names 
df = pd.DataFrame(data.data, columns=data.feature_names)

# Put the target (housing value -- MEDV) in another DataFrame
target = pd.DataFrame(data.target, columns=["MEDV"])

X = df["RM"]
y = target["MEDV"]

# Note the difference in argument order
model = sm.OLS(y, X).fit()
predictions = model.predict(X) # make the predictions by the model

# Print out the statistics
model.summary()
